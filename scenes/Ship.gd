extends Node2D

var owner_id = 0

sync var _accelerating = false
sync var speed = Vector2(100, 0)
sync var ship_angle = 0

const TICK = 1.0/60

const _ACCELERATION = 10

const _MAX_SPEED = 300
const _DELTA_SPEED_RED = 0.97
const _DELTA_ROT = 1.0/60*2*PI

func _ready():
	set_physics_process(true)
	$Booster.hide()
	add_to_group("actors")
	add_to_group("ships")

func _physics_process(delta):
	if _accelerating:
		$Booster.show()
		speed += _ACCELERATION*Vector2(cos(ship_angle), sin(ship_angle))
	
		if(abs(speed.x) > _MAX_SPEED):
			speed.x = sign(speed.x)*_MAX_SPEED
		if(abs(speed.y) > _MAX_SPEED):
			speed.y = sign(speed.y)*_MAX_SPEED
	else:
		$Booster.hide()
	
	set_position( get_position() + speed*delta )
	if Network.local_player_id == owner_id:
		rpc_unreliable("_sync_position", get_position())
	set_rotation(ship_angle)

remote func _sync_position(local_position):
	set_position(local_position)

func accelerate():
	rset("_accelerating", true)

func stop_acceleration():
	rset("_accelerating", false)

func deccelerate():
	rset("speed", _DELTA_SPEED_RED*speed)

func turn(dir):
	rset("ship_angle", ship_angle + sign(dir)*_DELTA_ROT)

func fire_brocoli():
	rpc("spawn_brocoli", get_position()+speed*0.1, get_rotation())

sync func spawn_brocoli(initial_position, angle):
	var laser = preload("res://scenes/Laser.tscn").instance()
	laser.set_rotation(angle)
	laser.update_speed()
	laser.set_position(initial_position+0.1*laser.speed)
	get_parent().add_child(laser)

func hurt():
	State.players_id_alive.erase(owner_id)
	State.did_we_lose()
	rpc("die")

sync func die():
	queue_free()

func set_owner(new_owner_id):
	State.players_id_alive.append(new_owner_id)
	owner_id = new_owner_id
	if owner_id != Network.local_player_id:
		$PlayerControl.queue_free()
