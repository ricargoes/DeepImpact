extends Popup

onready var Playerlist = $VBoxContainer/ItemList

func _ready():
	Playerlist.clear()

func refresh_players():
	Playerlist.clear()
	for player in Network.players.values():
		Playerlist.add_item(player["player_name"], null, false)

func _on_Ready_pressed():
	rpc("start_game")

sync func start_game():
	get_tree().change_scene("res://scenes/InGame.tscn")

func _on_Quit_pressed():
	get_tree().quit()
