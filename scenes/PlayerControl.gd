extends Node

var _ship

const _UI_UP = "ui_up"
const _UI_DOWN = "ui_down"  
const _UI_RIGHT = "ui_right"
const _UI_LEFT = "ui_left"
const _UI_FIRE = "ui_fire"
const _EXIT = "exit"

func _ready():
	set_process(true)
	set_process_input(true)
	_ship = get_parent()

func _process(delta):
	if(Input.is_action_pressed(_UI_LEFT)):
		_ship.turn(-1)
	elif(Input.is_action_pressed(_UI_RIGHT)):
		_ship.turn(1)
	if(Input.is_action_pressed(_UI_DOWN)):
		_ship.deccelerate()

func _input(event):
	if(event.is_action_pressed(_UI_UP)):
		_ship.accelerate()
	elif(event.is_action_released(_UI_UP)):
		_ship.stop_acceleration()
	if(event.is_action_pressed(_UI_FIRE)):
		_ship.fire_brocoli()

	if(Input.is_action_pressed(_EXIT)):
		get_tree().quit()
