extends Area2D

const MAX_SPEED = 400
const MAX_ANGULAR_SPEED = 2*PI
var asteroid_division = 2
var speed = 0.0
var angular_speed = 0.0

func _ready():
	add_to_group("actors")
	add_to_group("asteroids")
	asteroid_division = State.level*2
	
	set_process(true)

func _process(delta):
	set_position(get_position()+delta*speed)
	set_rotation(get_rotation()+delta*angular_speed)
	
	if get_tree().is_network_server():
		rpc_unreliable("_sync_position", position)
		if (get_overlapping_areas().size() > 0):
			for area in get_overlapping_areas():
				var thing = area.get_parent()
				if(thing.is_in_group("ships") and thing.has_method("hurt")):
					thing.hurt()

remote func _sync_position(local_position):
	set_position(local_position)

func initialize_movement():
	randomize()
	var angle = rand_range(0, 2*PI)
	var speed_module = rand_range(0, 1)*MAX_SPEED
	var ang_speed = rand_range(-1, 1)*MAX_ANGULAR_SPEED
	rpc("_set_asteroid_movement", speed_module, angle, ang_speed)

sync func _set_asteroid_movement(speed_module, angle, angular_speed):
	speed = Vector2(speed_module*cos(angle), speed_module*sin(angle))
	angular_speed = angular_speed

func hurt():
	for _i in range(0,asteroid_division):
		randomize()
		var angle = rand_range(0, 2*PI)
		var speed_module = rand_range(0, 1)*MAX_SPEED
		var ang_speed = rand_range(-1, 1)*MAX_ANGULAR_SPEED
		rpc("spawn_small_astteroid", speed_module, angle, angular_speed)
	rpc("die")

sync func die():
	queue_free()

sync func spawn_small_astteroid(speed_module, angle, angular_speed):
	var sa = load("res://scenes/SmallAsteroid.tscn").instance()
	sa.position = position
	sa.speed = Vector2(speed_module*cos(angle), speed_module*sin(angle))
	sa.angular_speed = angular_speed
	get_parent().add_child(sa)
	sa.asteroid_division = 0
