extends Node

func _ready():
	State.players_id_alive.clear()
	set_process(true)
	var n_asteroids = 2*State.level
	for _i in range(0,n_asteroids):
		spawn_asteroid()
	get_tree().paused = true
	add_to_group("waiting")
	var ships = $Ships.get_children()
	var player_list = Network.players.keys()
	player_list.sort()
	for ship in ships:
		if player_list:
			ship.set_owner(player_list.pop_front())
		else:
			ship.queue_free()
	Network.ready()

func resume():
	if get_tree().is_network_server():
		prepare_asteroids()
		rpc("resume_game")

sync func resume_game():
	remove_from_group("waiting")
	get_tree().paused = false

func prepare_asteroids():
	var asteroids = get_tree().get_nodes_in_group("asteroids")
	for asteroid in asteroids:
		asteroid.initialize_movement()

func _process(delta):
	var actors = get_tree().get_nodes_in_group("actors")
	for actor in actors:
		if(actor.has_method("get_position")):
			var pos = actor.get_position()
			var width = get_viewport().get_visible_rect().size.x
			var height = get_viewport().get_visible_rect().size.y

			var new_pos = pos
			if(pos.x > width):
				new_pos.x = pos.x - width
			elif(pos.x < 0):
				new_pos.x = pos.x + width
			if(pos.y > height):
				new_pos.y = pos.y - height
			elif(pos.y < 0):
				new_pos.y = pos.y + height

			if (new_pos != pos):
				actor.set_position(new_pos)
	if get_tree().is_network_server():
		State.did_we_win()

func spawn_asteroid():
	var asteroid = preload("res://scenes/BigAsteroid.tscn").instance()
	add_child(asteroid)
	return asteroid
