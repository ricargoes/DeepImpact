extends Node

var level = 1
const MAX_LEVEL = 5
var win = false
var active_scene

var players_id_alive = []

func did_we_lose():
	if not players_id_alive:
		rpc("lose")

func did_we_win():
	if (get_tree().get_nodes_in_group("asteroids").size()==0):
		rpc("victory")

sync func lose():
	State.win = false
	get_tree().change_scene("res://scenes/Aftergame.tscn")

sync func victory():
	State.win = true
	get_tree().change_scene("res://scenes/Aftergame.tscn")
