extends Control

func _on_Host_pressed():
	var port = $VBoxContainer/HBoxContainer/HostCont/Port.text
	var player_name = $VBoxContainer/HBoxContainer/HostCont/Name.text
	Network.player_data["player_name"] = player_name
	Network.create_server(port)
	create_waiting_room()


func _on_Join_pressed():
	var ip = $VBoxContainer/HBoxContainer/JoinCont/IP.text
	var port = $VBoxContainer/HBoxContainer/JoinCont/Port.text
	var player_name = $VBoxContainer/HBoxContainer/JoinCont/Name.text
	Network.player_data["player_name"] = player_name
	Network.connect_to_server(ip, port)
	$WaitingRoom/VBoxContainer/HBoxContainer/Ready.queue_free()
	create_waiting_room()

func create_waiting_room():
	$WaitingRoom.popup_centered()
	$WaitingRoom.refresh_players()
