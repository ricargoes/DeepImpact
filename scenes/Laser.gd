extends Node2D

const _SPEED_MODULUS = 800

var speed = Vector2(0,0)
var countdown = 1

func _ready():
	set_physics_process(true)
	add_to_group("actors")

func _physics_process(delta):
	position += speed*delta
	countdown -= delta

	if(countdown <= 0):
		queue_free()
	
	if get_tree().is_network_server():
		rpc_unreliable("_sync_position", position)
		if (get_node("Area2D").get_overlapping_areas().size() > 0):
			for thing in get_node("Area2D").get_overlapping_areas():
				if(thing.is_in_group("asteroids") and thing.has_method("hurt")):
					thing.hurt()
					rpc("die")

func update_speed():
	speed = _SPEED_MODULUS*Vector2(cos(rotation), sin(rotation))

remote func _sync_position(local_position):
	set_position(local_position)

sync func die():
	queue_free()
