extends Node

var local_player_id = 0
sync var players = {}
sync var player_data = {}
var players_ready = []

signal player_disconnected
signal server_disconnected

func _ready():
	get_tree().connect("network_peer_disconnected", self, "_on_player_disconnected")
	get_tree().connect("network_peer_connected", self, "_on_player_connected")

func create_server(port):
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(int(port), 2)
	get_tree().set_network_peer(peer)
	add_to_player_list()

func connect_to_server(ip, port):
	var peer = NetworkedMultiplayerENet.new()
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	peer.create_client(ip, int(port))
	get_tree().set_network_peer(peer)

func add_to_player_list():
	local_player_id = get_tree().get_network_unique_id()
	players[local_player_id] = player_data

func _connected_to_server():
	add_to_player_list()
	rpc("_send_player_info", local_player_id, player_data)
	
remote func _send_player_info(id, player_info):
	players[id] = player_info
	if get_tree().is_network_server():
		rset("players", players)
		rpc("update_network_info")

func _on_player_connected(id):
	if not get_tree().is_network_server():
		print( str(id) + " has connected")


func _on_player_disconnected(id):
	players.erase(id)
	if get_tree().is_network_server():
		rset("players", players)
		rpc("update_network_info")


sync func update_network_info():
	get_tree().call_group("network_info", "refresh_players")

func ready():
	rpc("_set_player_ready", local_player_id)

sync func _set_player_ready(id):
	if not id in players_ready:
		players_ready.append(id)
	
	if players_ready.size() == players.keys().size():
		_clear_players_ready()
		get_tree().call_group("waiting", "resume")

func _clear_players_ready():
	players_ready = []
